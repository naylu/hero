package com.example.hero.ui.hero

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.hero.R
import com.example.hero.data.entities.Hero
import com.example.hero.data.local.Constant
import com.example.hero.databinding.ItemHeroBinding
import com.example.hero.databinding.LoadingProgressBinding
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation



class HeroAdapter(private val listener: HeroItemListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class LoadingViewHolder(itemBinding: LoadingProgressBinding) : RecyclerView.ViewHolder(itemBinding.root)

    interface HeroItemListener {
        fun onClickedHero(heroId: String)
    }

    private val items = ArrayList<Hero?>()

    fun addItems(items: ArrayList<Hero>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setItems(items: ArrayList<Hero>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun cleanItems(){
        this.items.clear()
        notifyDataSetChanged()
    }

    fun addLoadingView() {
        //add loading item
        Handler().post(){
            items.add(null)
            notifyItemInserted(items.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        if (items.size != 0) {
            items.removeAt(items.size - 1)
            notifyItemRemoved(items.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val binding: ItemHeroBinding = ItemHeroBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            HeroViewHolder(binding, listener)
        } else {
            val binding : LoadingProgressBinding = LoadingProgressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                binding.progressbar.indeterminateDrawable.colorFilter = BlendModeColorFilter(R.color.colorPrimaryDark, BlendMode.SRC_ATOP)
            } else {
                binding.progressbar.indeterminateDrawable.setColorFilter(ContextCompat.getColor(parent.context, R.color.colorPrimaryDark), PorterDuff.Mode.MULTIPLY)
            }
            LoadingViewHolder(binding)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return if (items[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int){
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
          return  (holder as HeroViewHolder).bind(items[position]!!)
        }
    }
}

class HeroViewHolder(private val itemBinding: ItemHeroBinding, private val listener: HeroAdapter.HeroItemListener) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var hero: Hero

    init {
        itemBinding.root.setOnClickListener(this)
    }

    @SuppressLint("SetTextI18n")
    fun bind(item: Hero) {
        val picasso = Picasso.get()
        this.hero = item
        itemBinding.name.text= item.name
        itemBinding.speciesAndStatus.text = if(item.appearance.race == "null") "Undefined" else item.appearance.race
        picasso.load(item.image.url).placeholder(R.drawable.placeholder_hero).transform(CropCircleTransformation()).into(itemBinding.image)
    }

    override fun onClick(v: View?) {
        listener.onClickedHero(hero.id)
    }
}