package com.example.hero.ui.herodetail

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.hero.R
import com.example.hero.data.entities.Hero
import com.example.hero.databinding.FragmentHeroDetailBinding
import com.example.hero.utils.Resource
import com.example.hero.utils.autoCleared
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import jp.wasabeef.picasso.transformations.CropCircleTransformation

@AndroidEntryPoint
class HeroDetailFragment : Fragment() {

    private var binding: FragmentHeroDetailBinding by autoCleared()
    private val viewModel: HeroDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHeroDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString("id")?.let { viewModel.start(it) }
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.hero.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    bindHero(it.data!!)
                    binding.progressBar.visibility = View.GONE
                    binding.heroCl.visibility = View.VISIBLE
                }

                Resource.Status.ERROR ->
                    Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.progressBar.indeterminateDrawable.setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.MULTIPLY)
                    binding.heroCl.visibility = View.GONE
                }
            }
        })
    }

    private fun bindHero(hero: Hero) {
        val picasso = Picasso.get()
        binding.name.text = hero.name
        binding.species.text = if(hero.appearance.race == "null") "Undefined" else hero.appearance.race
        binding.status.text = hero.powerstats.power
        binding.gender.text = hero.appearance.gender
        picasso.load(hero.image.url).placeholder(R.drawable.placeholder_hero).transform(CropCircleTransformation()).into(binding.image)
    }
}