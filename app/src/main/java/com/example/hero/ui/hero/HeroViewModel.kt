package com.example.hero.ui.hero
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.hero.data.entities.HeroList
import com.example.hero.data.repository.HeroRepository
import com.example.hero.utils.Resource


class HeroesViewModel @ViewModelInject constructor(
    private val repository: HeroRepository
) : ViewModel() {

    fun getHeroes(skip: Int, limit: Int): LiveData<Resource<HeroList>> {
        return repository.getHeroes(limit = limit, skip = skip)
    }

    fun getHeroWithName(name: String): LiveData<Resource<HeroList>>{
        return repository.getHeroWithName(name = name)
    }
}