package com.example.hero.ui.hero

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hero.R
import com.example.hero.databinding.FragmentHeroBinding
import com.example.hero.utils.RecyclerViewLoadMoreScroll
import com.example.hero.utils.Resource
import com.example.hero.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_hero.view.*

@AndroidEntryPoint
class HeroFragment : Fragment(), HeroAdapter.HeroItemListener {
    
    private var binding: FragmentHeroBinding by autoCleared()
    private val viewModel: HeroesViewModel by viewModels()
    private lateinit var adapter: HeroAdapter
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
     var screenSearch: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHeroBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        loadMoreData()
        view.btn_search.setOnClickListener(){
            onClickSearch()
        }
        view.btn_clean.setOnClickListener(){
            onClickClean()
        }
    }

    private fun setupRecyclerView() {
        adapter = HeroAdapter(this)
        mLayoutManager = LinearLayoutManager(requireContext())
        binding.heroesRv.adapter = adapter
        setRVLayoutManager()
        setRVScrollListener()
    }

    private fun setRVLayoutManager() {
        binding.heroesRv.layoutManager = mLayoutManager
        binding.heroesRv.setHasFixedSize(true)
    }

    private  fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager as LinearLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            RecyclerViewLoadMoreScroll.OnLoadMoreListener {
            override fun onLoadMore() {
                if(!screenSearch)loadMoreData()
            }
        })
        binding.heroesRv.addOnScrollListener(scrollListener)
    }

    private fun loadMoreData() {
        adapter.addLoadingView()
        val skip = adapter.itemCount
        viewModel.getHeroes(skip= skip, limit = 8).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    adapter.removeLoadingView()

                    adapter.addItems(ArrayList(it.data!!.results))

                    scrollListener.setLoaded()

                    binding.heroesRv.post {
                        adapter.notifyDataSetChanged()
                    }
                }
                Resource.Status.ERROR ->{
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onClickedHero(heroId: String) {
        findNavController().navigate(
            R.id.action_heroFragment_to_heroDetailFragment,
            bundleOf("id" to heroId)
        )
    }

    private fun onClickSearch(){
        if(binding.textSearch.text.toString() != "") screenSearch = true
        viewModel.getHeroWithName(binding.textSearch.text.toString()).observe(viewLifecycleOwner, Observer {
            when (it.status){
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE



                    if(!it.data!!.results.isNullOrEmpty()) {
                        if(it.data.results.size == 1){
                            findNavController().navigate(
                                R.id.action_heroFragment_to_heroDetailFragment,
                                bundleOf("id" to it.data.results.elementAt(0).id)
                            )
                        }else{
                            adapter.setItems(ArrayList(it.data!!.results))
                        }
                    } else {
                        adapter.cleanItems()
                    }

                }
                Resource.Status.ERROR ->{
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun onClickClean(){
        screenSearch = false
        binding.textSearch.setText("")
        adapter.cleanItems()
        loadMoreData()
    }
}