package com.example.hero.ui.herodetail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.example.hero.data.entities.Hero
import com.example.hero.data.repository.HeroRepository
import com.example.hero.utils.Resource

class HeroDetailViewModel @ViewModelInject constructor(
    private val repository: HeroRepository
) : ViewModel() {

    private val _id = MutableLiveData<String>()

    private val _hero = _id.switchMap { id ->
        repository.getHero(id)
    }
    val hero: LiveData<Resource<Hero>> = _hero


    fun start(id: String) {
        _id.value = id
    }

}