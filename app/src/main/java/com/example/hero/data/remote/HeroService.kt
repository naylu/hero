package com.example.hero.data.remote

import com.example.hero.data.entities.Hero
import com.example.hero.data.entities.HeroList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HeroService {

    @GET("{id}")
    suspend fun getHero(@Path("id") id: String): Response<Hero>

    @GET("search/{name}")
    suspend fun getHeroWithName(@Path("name") name: String): Response<HeroList>
}