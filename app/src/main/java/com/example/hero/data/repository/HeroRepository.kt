package com.example.hero.data.repository

import com.example.hero.data.entities.Hero
import com.example.hero.data.entities.HeroList
import com.example.hero.data.entities.HeroListInfo
import com.example.hero.data.remote.BaseDataSource
import com.example.hero.data.remote.HeroService
import com.example.hero.utils.performGetOperation
import retrofit2.Response
import javax.inject.Inject
import kotlin.math.min

class HeroRepository @Inject constructor(
    private val heroService: HeroService
) : BaseDataSource(){

    fun getHero(id: String) = performGetOperation(
        networkCall = {  getResult { heroService.getHero(id) }}
    )

    private suspend fun listHeroes(search: String = "", limit: Int = 10, skip: Int = 0) : Response<HeroList> {
        val heroesCount = 731
        val from = min(skip + 1, heroesCount)
        val to = min(from + limit - 1, heroesCount)
        val heroes = mutableListOf<Hero>()
        for (id in from..to){
                val response = heroService.getHero(id.toString())
                if (response.isSuccessful) {
                    val body = response.body()
                    if (body != null) {
                        heroes.add(body)
                    }
                } else {
                    return Response.error(response.code(), response.errorBody()!!)
                }
        }
        val heroList = HeroList(results = heroes.toList(), info = HeroListInfo(count = heroesCount), response = null)
        return Response.success(heroList)
    }

    fun getHeroes(search: String = "", limit: Int = 10, skip: Int = 0) = performGetOperation(
        networkCall = {
            getResult {
                listHeroes(limit = limit, skip = skip, search = search)
            }
        }
    )

    fun getHeroWithName(name: String) = performGetOperation(
        networkCall = {  getResult { heroService.getHeroWithName(name) }}
    )
}