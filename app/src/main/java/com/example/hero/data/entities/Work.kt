package com.example.hero.data.entities

data class Work (
    val occupation: String,
    val base: String
)