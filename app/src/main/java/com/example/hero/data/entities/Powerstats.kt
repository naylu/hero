package com.example.hero.data.entities

data class Powerstats (
val intelligence: String,
val strength: String,
val speed: String,
val durability: String,
val power: String,
val combat: String
)