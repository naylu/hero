package com.example.hero.data.entities

data class HeroList(
    val results: List<Hero>,
    val info: HeroListInfo?,
    val response: String?
)