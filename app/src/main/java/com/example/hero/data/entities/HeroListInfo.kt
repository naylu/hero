package com.example.hero.data.entities

data class HeroListInfo(
    val count: Int
)