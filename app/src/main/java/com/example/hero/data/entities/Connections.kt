package com.example.hero.data.entities

data class Connections (
    val groupAffiliation: String,
    val relatives: String
)