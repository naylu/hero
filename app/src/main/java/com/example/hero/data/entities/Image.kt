package com.example.hero.data.entities

data class Image (
    val url: String
)